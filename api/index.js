import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import userRouter from "./routes/user.route.js";
import authRouter from "./routes/auth.route.js";
dotenv.config();

mongoose
  .connect(
    "mongodb+srv://batzorig:YKZiLLTCYR8pFZ4G@mern-ten-hour.rf6htgk.mongodb.net/?retryWrites=true&w=majority"
  )
  .then(() => {
    console.log("Connected to MongoDB");
  })
  .catch((err) => {
    console.log(err);
  });

const app = express();
app.use(express.json());

app.listen(3000, () => {
  console.log("server is running 3000");
});

app.use("/api/user/", userRouter);
app.use("/api/auth", authRouter);
